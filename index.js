var ndef = require('ndef');
var fs = require('fs')
var async = require('async')
var exec = require('child_process').exec;
var ld = require('node-ld')

// Monkeypatch to allow converting buffer to array
Buffer.prototype.toByteArray = function () { return Array.prototype.slice.call(this, 0) }

var direction = {
	INCOMING: 0,
	OUTGOING: 1,
}

var toypad = new ld.ToyPad()
toypad.on('ready', function main() {
  toypad.wake((e,d) => {
              var disabled = 0;
              toypad.pwd(disabled);
             })
})

toypad.on('event', function incomingEvent(ev) {
	switch (ev.dir) {
    case direction.INCOMING:
  		dumpTag(ev.uid, ev.pad, ev.index);
      break;
    case direction.OUTGOING:
      break;
	}
})

function dumpTag(uid, pad, index){
	var TAGSIZE = 180
	var PAGESPERREAD = 4
	var PAGESIZE = 4
	var PAGECNT = TAGSIZE/PAGESIZE
	var tasks = []
	var b = new Buffer(TAGSIZE)
	b.fill(0)

	for(var page = 0; page < PAGECNT; page += PAGESPERREAD) {
		tasks.push(page)
	}

	var cnt = PAGECNT
	var worker = function(page, workerCallback){
		toypad.read(index, page, (err, resp) => {
			resp.payload.slice(1).copy(b, page*PAGESPERREAD)
			workerCallback()
		})
	}

	async.eachLimit(tasks, 5, worker, function() {
    dumpComplete(b);
	})
}

function dumpComplete(data){
  var userMemory = data.slice(4*4); //4 header pages at start of tag

  do {
    var type = userMemory[0]
    var length = userMemory[1]
    var value = userMemory.slice(2, 2+length).toByteArray();

    switch(type) {
      case 0: //NULL
        console.log("TLV.NULL");
        break;
      case 1: //LockControl
        console.log("TLV.LockControl");
        break;
      case 2: //MemControl
        console.log("TLV.MemControl");
        break;
      case 3: //NDEF
        console.log("TLV.NDEF");
        //ndef.stringify(value);
        var message = ndef.decodeMessage(value);
        handleNdefMessage(message);
        break;
      case 0xFE: //Terminator
        console.log("TLV.Terminator");
        return;
    }

    userMemory = userMemory.slice(length + 2);
  } while (userMemory.length > 0);

  console.log("parsing complete")
}

function handleNdefMessage(message) {
  message.forEach(function(record) {
    if (record.tnf == ndef.TNF_WELL_KNOWN) {
      wktAction(record);
    }
  });
}

function wktAction(record) {
  var command = '';
  switch(record.type) {
    case ndef.RTD_TEXT:
      var s = ndef.text.decodePayload(record.payload);
      command = 'echo "' + s + '" | pbcopy';
      break;
    case ndef.RTD_URI:
      var uri = ndef.uri.decodePayload(record.payload);
      command = 'open ' + uri;
      break;
    case ndef.RTD_SMART_POSTER:
      handleNdefMessage(ndef.decodeMessage(record.payload));
      break;
  }
  if (command.length > 0) {
    console.log("running: ", command);
    exec(command);
  }
}



